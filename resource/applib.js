//var availElements={a:{},b:{},div:{},html:{},img:{}},t={d:'div',s:'span',nf:'node_file',nv:'node_elementV',nh:'node_elementH',nu:'node_undefinedHTML',na:'nodeHead',nb:'nodeBody',nc:'nodeFoot'};
Object.defineProperty(window,"log",{get:function(){return "Yeh, I can keep a secret.";},//This will not keep a secret
                               set:function(a){console.log(a)},
                                //Sends anything it is set to the console
                               configurable : false});
var tl/*tools*/={
    //Tools object, contains shorthand functions
    //Type checkers
    iS:function(a){return typeof a=="string"},
    iF:function(a){return typeof a=="function"},
    iO:function(a){return typeof a=="object"},
    iN:function(a){return typeof a=="number"},
    mD:function(){var a=document.createElement("div"),i=0;for(i=0;i<arguments.length;i++)a.classList.add(arguments[i]);return a},//Makes divs, adds to classList from arguments
    mS:function(){return document.createElementNS("http://www.w3.org/2000/svg",arguments[0])},
    mR:function(a){return Math.floor(Math.random()*Math.pow(10,a||3))}
    //Makes random number to power of Argument
},
st={c:"class",p:"px",e:"expands",a:"active"/*String shorthand for those oodles of calls*/},
tIDE={//Global App Object
    init:function(fileRef){
        //Global Initializer
        var fileName=fileRef||"untitled";
        //$('#vpCont').appendChild((tIDE.openTabs[fileName]=tIDE.focus=tIDE.envNode.spawn.call({},3)).host);
        tIDE.focus.init();
    },
    session:{//Contains all pertainant session data
        fog:{//Specific to fog server
            loggedIn:{},
            sendReq:function(req,callback){
                var http=new XMLHttpRequest();
                http.open("POST","/tide",true);
                http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                http.onreadystatechange = function(){
                        if(http.readyState==4&&http.status==200){
                            callback(http.response);}}
                http.send(req.join("&"))}}},
    io:{
        //Contains all the functions and objects pertainant to interface
        init:function(type){//menu & workspace initializer
            if(!type){
                var t=this;
                t.updatesvg();
                t.updatecld();
                t.menu.init();
            }
        },
        login:{//Login applet
            init:function(){
                var a=this.local;
            }
        },
        updatesvg:function(){
            //Itterates over svg placeholders set up with .innerHTML and sets up as clones of nodes stored in an <aside>
            //I gotta fix now that I figured out <use> exists >:|
            var a,b,c,d="";
            for(i=(c=document.getElementsByClassName("svgph")).length;a=c[--i];){
                if(b=document.getElementsByClassName("svg_"+(a.classList[1]||null))[0])a.parentNode.replaceChild(b=b.cloneNode(true),a)
                if((d=a.getAttribute(st.c).indexOf(" ",6))+1){
                    b.setAttribute(st.c,b.getAttribute(st.c).concat(a.getAttribute(st.c).slice(d)))
                }
            }
        },
        updatecld:function(){
            //Itterates over elements set up with .innerHTML and files them under respective JS objects based upon title attribute
            var b=document.getElementsByClassName("_node"),level;
            for(var i=b.length-1;i>=0;--i){
                var a=b[i],elem=a,res=[],m=false,c;
                pLoop:for(var p=0;a.title&&p<15;p++){
                    var e=a.title.split("."),lRes=[],o=0;
                    for(var n=e.length-1;n>=0;--n){
                        var f=e[n];
                        if(/^[\$\/\!\&]/.test(f)){
                            var g=f.substring(0,1);
                            f=f.substring(1);
                            //if(/^[\*]/.test(f[1]))f=this.cldSH[f.substring(1)];
                            if(g=="$"||g=="/"){
                                //Way sexier & suprisingly won in JSperf by 63% >:D
                                Array.prototype.unshift.apply(res,m==true?f:[f,"node"]);
                                /*if(m)resolution.unshift("node");
                                resolution.unshift(f);*/
                                if(g=="$")break pLoop;
                                else m=true;
                            }
                            if(g=="&"||g=="!"){
                                if(!isNaN(f)){
                                    for(f=parseFloat(f)+1;o<f;o++){a=a.parentNode};
                                    //NEED LEXICAL SCOPING, FFS.
                                    Array.prototype.unshift.apply(res,lRes);
                                    continue pLoop;
                                }
                                else if(p==0){
                                    lRes=[f];
                                    if(g=="!")m=1;
                                }
                                else continue
                            }
                        }
                        else lRes.unshift(f);
                    }
                    Array.prototype.unshift.apply(res,lRes);
                    if(!o){a=a.parentNode};
                }
                if(!m)res.splice(1,0,"node");
                var f=res.pop(),d=tIDE;
                for(var j=0;c=res[j];j++){d=d[c]=(d.tagName?d={parent:d}:d)[c]||{}};
                if(d[f])d[f].parent=elem;else d[f]=elem;
            }
            for(var i=b.length-1;i>=0;--i){
                b[i].removeAttribute("title");
                b[i].classList.length-1?b[i].classList.remove("_node"):b[i].removeAttribute("class");
            }
            return
        },
        menu:new function(){
            //menu object containing methods and data pertinent to the menu's functionality
            var t=this;
            //Set up as an anonymous constructor for the purpose of passing "this" within closures
            t.init=function(){
            //Ties menu events to top-level nodes
                var i=t.node.item,list=tIDE.settings.menuList,c=t.node.parent;
                for(a in i){
                    i[a].level=0;
                    i[a].cList=list[a];
                    i[a].handle={click:1,mouseover:1}}
				var i=t.node.expands;
					i.tIDE.handle={click:102};
					i.Login.handle={click:101};
                t.active[-1]={cList:list};
                c.addEventListener("click",t.onEvent);
                c.addEventListener("mouseover",t.onEvent);
                c.classList.remove("off");
            };
            t.active=[];
            t.gCollect=function(p,e){
                //Deletes all menu levels above the one to be refreshed; also kills container if menu is deinitialized
                var a=t.active,p=(e=event||e)&&e.target||p,level=a.length,pL=p.level||0;
                if(level>pL){
                    for(i=pL;i<level;i++){
                    t.node.menuEnv.removeChild(a[i].cGroup);
                    a[i].classList.remove(st.a);
                    delete a[i].cGroup}
                a[pL].classList.remove(st.a);
                a.splice(pL,level-pL)}
                if(e&&e.type=="click"){
                    tIDE.io.node.overlay.removeChild(t.node.menuEnv);
                    delete t.node.menuEnv}};
            t.initCont=function(event){
                //Creates the container for menu popouts and positions them accordingly
                if(t.node.menuEnv)return;
                var aMenu=t.node.menuEnv=tl.mD();
                aMenu.id="aMenuCont";
                (aMenu.appendChild(t.node.offsetLeft=tl.mD())).classList.add("offsetLeft");
                tIDE.io.node.overlay.appendChild(aMenu);
                aMenu.addEventListener("click",t.onEvent);
                aMenu.addEventListener("mouseover",t.onEvent);
                aMenu.handle={click:2};
                t.node.container.handle={click:2};
                t.initGroup(event)};
            t.initGroup=function(event){
                //Triggered by events fired by menu elements, constucts relevant submenu and appends to container
                if(!t.node.menuEnv)return
                var a=t.active,p=event.target,level=a.length,pL=p.level||0,et=t.node.menuEnv;
                if(level>pL)t.gCollect(p);
                if(pL>1)a[pL-1].classList.add(st.a);
                /*if(!p.classList.contains(st.e)&&pL>0){return}*/
                var group=tl.mD("group"),buffer=tl.mD("buffer");
                if(pL==0){t.node.offsetLeft.style.width=p.offsetLeft+st.p}
                else{var oT=group.appendChild(tl.mD("offsetTop"));
                    oT.style.height=p.offsetTop+st.p;
                    oT.style.maxHeight=p.offsetTop+st.p;
                    oT.handle={click:2}}
                group.appendChild(buffer);
                for(i=0,d=p.cList.length;i<d;i++){
                    var k=p.cList[i];
                    if(i)(buffer.appendChild(document.createElement("hr"))).setAttribute(st.c,"splitter");
                    for(n in k){
                        var item=tl.mD();
                        item.handle={};
                        item.innerText=n;
                        if(tl.iO(k[n])&&tl.iO(k[n][0])){
                            var ctx=item.appendChild(tl.mD("ctx"));
                            ctx.appendChild(tl.mD("svgph","popout"));
                            item.classList.add(st.e);
                            item.handle.mouseover=1;
                            item.cList=k[n]
                        }
                        else if(tl.iN(k[n])){
                            item.classList.add("clickable");
                            item.handle={mouseover:2,click:3};
                        }
                        item.level=pL+1;
                        item.classList.add("item");
                        buffer.appendChild(item)
                    }
                }
                p.cGroup=group;
                if(!pL)p.classList.add(st.a);
                a.push(p);
                t.node.menuEnv.appendChild(group);
                group.handle={click:2};
                p.classList.add(st.a);
                tIDE.io.updatesvg();
            }
            t.onEvent=function(){
                //Reads event.targets event references and corresponds them with list items to run appropriate method; kills propogation, thus limiting to objects in question.
                var a=t.handles[event.type],b=event.target.handle;
                event.stopPropagation();
                if((b=(typeof b=="object")&&b[event.type])&&typeof (a=a&&a[b])=="function")a(event);
                return;
            };
            t.status={
                //Contains information that is edited upon app state change; menu constructor read these to determine exclusion, greyout, and (un)checked if applicable
                loggedin:0,
                loggedinFog:0,
                svgActive:0,
                htmlActive:0
            };
            t.handles={
                //List of functions tied to event firings
                click:{
                    0:function(){return false},
                    1:t.initCont,
                    2:t.gCollect,
					3:function(){
						var a=null;
						typeof (a=tIDE.io.menu.handles.buttons[event.target.innerText])=="function"&&a();
					},
                    101:function(event){console.log(event.target.innerHTML)},
					102:function(event){
						tIDE.io.workspace.node.parent.style.webkitTransform="translateX(100px)";
						tIDE.io.node.overlay.classList.add("menuLeft");
					}
                },
                mouseover:{
                    0:function(){return false},
                    1:t.initGroup,
                    2:t.gCollect
                },
				buttons:{
					
				}
            };   
        },
        workspace:new function(){t=this;
            t.tabs={};
            t.init=function(){
                //this.node.parent.addEventListener("click",this.handleClick);
            },
            t.fTab=null;
            t.cFocus=function(instance){
                var t=this,a=null,cS=0;
                instance.focus();
				t.fTab=instance;
                //if(!instance.workspace==t.focus.workspace){}
                //t.node[1].replaceChild(instance[1],t.node[1]);
				
				(a=t.node[1]).firstElementChild&&a.removeChild(a.firstElementChild);
				a.appendChild(instance[1]);
				if(instance.paint)t.paint();
				return
				
                var ws=tIDE.settings.workspace._svg;
				/*
                if(!t.cWS===instance.layout){
                    for(var i=0;i<ws.length;i++){
                        var a=ws[i];
                        if(typeof a=="string"){ws=workspace.layout[a]||[];i=0;continue}
                        if(typeof a.fI=="number"?cS=a.fl:0||a.cW){
                            //cG=t.auxConstructors[a.fI];
                            cG=t[cS].appendChild(tl.mD("group"));
                            cG.style.minWidth=a.cW;
                        }
                        if(a=tIDE.svg.ioConstruct[a.type])cG.appendChild(new a(instance));
                    }
                }
                t.cLayout=tIDE.settings.wsLayout[ws[0]]
				*/

            },
            t.newTab=function(info){
                var t=this;
                while((info.idn=tl.mR(1)) in t.tabs);
				
				//something=new tIDE.svg.Instance(info);
				
                var instance=t.tabs[info.idn]=new tIDE[info.type].Instance(info);
				
				tIDE.svg.Instance(info);
                t.node.tabzone.appendChild(instance.tab);
                workspace=instance.wsLayout
                t.cFocus(instance);
            },
            t.onEvent=function(){
                //Reads event.targets event references and corresponds them with list items to run appropriate method; kills propogation, thus limiting to objects in question.
                var a=t.handles[event.type],b=event.target.handle;
                event.stopPropagation();
                if((b=(typeof b=="object")&&b[event.type])&&typeof (a=a&&a[b])=="function")a(event);
                return;
            },
            t.handles={
                //List of functions tied to event firings
                click:{
                    0:function(){return false},
                    101:function(event){console.log(event.target.innerHTML)}
                },
                mouseover:{
                    0:function(){return false},
                }
            },
            t.focus=function(){};
			t.paint=function(){
				t.node.canvas.width=t.node.canvasCont.clientWidth;
				t.node.canvas.height=t.node.canvasCont.clientHeight;
				t.fTab.paint();
			};
        },
        filebrowser:{
            //applet spawned by File.Open; --NOT YET IMPLEMENTED--
            init:function(parent,type){
                //this.host=new this.FileBrowser;
            },
            FileBrowser:function(){
                var dom;
                if(tIDE.session.lastState.filebrowser){}
                    dom=document.createElement('div');
                    dom.innerHTML='<div id="fileBrowser"><div class="fbcontainer"><div class="fRow"><div class="fRowScrollSpring"><ul class="fRowCont"></ul></div><div class="fRowBorder"></div></div></div></div>';
                if(tIDE.session.fog.loggedIn){
                    
                }
            },
            fologinS:function(a){
                sendReq([Math.floor(Math.random()*10000)+":"+"lsplz="+a],
                function(res){
                    res=JSON.parse(res)
                });
            },
            dRows:[]
        }
    },
    parseXML:function(text,constructor,callback){
        text=JSON.parse(text)[text.substring(2,6)]
        var pChunk=[],sA=text.split(/\</),e;
        sA.forEach(function(chunk,i){
            var cp=chunk.match(/\S+/),tags=constructor.tags;
            pChunk[i]=(tags[cp]||constructor.validTags[cp]?tags._std:0||tags._err)(chunk[i])
        });not
        console.log(sA);
        return 0;
    },
	html:new function(){
		var _g=this,_n=_g.Nodes={},aC=t.ioConstruct={},_=function(obj){for(r in obj){this[r]=obj[r]}};
		_.prototype={
			findNext:function(){
				if(this.contains&&this.children.length){
					return this.children[0];
				}
				else if(!this.next){
					var a=null;
					
					return a;
				}
				else return this.next;
			},
			findPos:function(){
				
			}
		};
		(_n.div=function(s){
		
		
		}).prototype=new _({type:"div",contains:true,
			paint:function(context,aStack){
				var sub=this;
				aStack.unshift(new aStack[-1](1,function(){
					return sub.parent.next
				}))
				return this.findNext();
			},
			findBounds:function(){
				
			}
		});
	},
    svg:new function(){//SVG editing environment
        var _g=this,_n=_g.Nodes={},aC=t.ioConstruct={},_=function(obj){for(r in obj){this[r]=obj[r]}};
		_.prototype={
			findNext:function(){
				if(this.contains&&this.children.length){
					return this.children[0];
				}
				else if(!this.next){
					var a=null;
					
					return a;
				}
				else return this.next;
			},
			findPos:function(){
				
			}
		};
        /*_g.tags={//stuff for parser to custom read certain things like comments and doctype
            "?xml":function(chunk){},
            "!--":function(){},
            _std:function(a){
                var slices=a.split(/\s+/)
                this.type=slices.shift();},
            _err:function(){}, 
            "":function(){}
        }
	   aC[0]={
			viewport:function(i){
				var viewport=_g[1]=tl.mD(),
				overlay=viewport.appendChild(t.overlay=tl.mD("overlay")),
				host=viewport.appendChild(t.overlay=i.host)
				return 
			}
		};
		aC[1]=aC[2]={
			layers:function(v){
				var a=tl.mD("layers","aux");
				return a;
			}
		};
		aC[3]={
			status:function(v){

			}
		};*/
		//SVG node constructs
		(_n.g=function(s){
		}).prototype=new _({type:"g",contains:true,
			paint:function(context,aStack){
				var sub=this;
				aStack.unshift(new aStack[-1](1,function(){
					return sub.parent.next
				}))
				return this.findNext();
			},
			findBounds:function(){
				
			}
		});
		(_n.path=function(s){
			var t=this;

			t.points=s.pathSegList;
			t.array=[];
			for(var i=0,j=t.points.numberOfItenms;i<j;i++)t.array.push(t.points.getItem(i));

		}).prototype=new _({type:"path"});
		(_n.polygon=function(){
		}).prototype=new _({type:"polygon"});
		(_n.rect=function(s){
			var t=this;
			t.stat=[s.x.baseVal.value,s.y.baseVal.value,s.width.baseVal.value,s.height.baseVal.value,s.rx.baseVal.value,s.ry.baseVal.value];
			t.layer=tl.mD("g","svg_layer");
		}).prototype=new _({type:"rect",
			paint:function(context,aStack){
				//context.rect.apply(context,this.stat.slice(0,4))
				t=this;
				if(this.animation.length){
					for(var i=0;i<this.animation.length;i++){
						
					}
				}
				context.rect(t.stat[0],t.stat[1],t.stat[2],t.stat[3])
				context.fillStyle="#333";
				context.fill();
				return this.findNext();
			}
		
		
		});
		(_n.ellipse=function(){}).prototype=new _({type:"ellipse"});
		(_n.circle=function(s){
			var t=this;
			t.stat=[t.cx=s.cx.baseVal.value,t.cy=s.cy.baseVal.value,t.r=s.r.baseVal.value];

		}).prototype=new _({type:"circle"});
		(_n.text=function(){}).prototype=new _({type:"text"});
		(_n.image=function(){}).prototype=new _({type:"image"});
		(_n.line=function(){}).prototype=new _({type:"line"});
		(_n.linearGradient=function(){if(oStack[0].type="defs"){
			}return sub.nextElementSibling
		}).prototype=new _({type:"linearGradient"});
		(_n.defs=function(){}).prototype=new _({type:"defs",contains:true});
		(_n.desc=function(){}).prototype=new _({type:"desc",contains:true});
		(_n.metadata=function(){}).prototype=new _({type:"metadata",contains:true});
		(_n.polyline=function(){}).prototype=new _({type:"polyline"});
		(_n.stop=function(){}).prototype=new _({type:"stop"});
		(_n.style=function(){}).prototype=new _({type:"style"});
		(_n["switch"]=function(){}).prototype=new _({type:"switch"});
		(_n.symbol=function(){}).prototype=new _({type:"symbol"});
		(_n.tref=function(){}).prototype=new _({type:"tref"});
		(_n.tspan=function(){}).prototype=new _({type:"tspan"});
		(_n.title=function(){}).prototype=new _({type:"title"});
		(_n["use"]=function(){}).prototype=new _({type:"use"});
		(_n.mask=function(){}).prototype=new _({type:"mask",contains:true});
			//_n.selectionTop
        //Constructor for SVG editing environment that stores opperation methods and document data in local memory
        (_g.Instance=function(info){
            //declaration
            var t=this,viewport=t.viewport;//,t.ws=tIDE.io.workspace;
            t.children=[];
			t.ws=tIDE.io.workspace;
			t.cStat={
				zoom:1,
				oX:0,
				oY:0
			};
			t.local={
				//animate:{},
				status:{}
			};
			t.settings={ruler:true};
            if(info.source){
				deConstruct(t,info)
				
			}
            else {

            }
            var tE=t.tab=tl.mD("tab");
            tE.innerText=t.name;
            tE.host=t;
		
			t[1]=t.ws.node.canvasCont=tl.mD("viewport","svg");
			t[1].appendChild(tIDE.io.workspace.node.canvas=document.createElement("canvas"));
 
			function getBounds(){
				var sub=s.children[0],a=null;
				for(var i=0;sub&&i<10;i++){
					sub.findPos&&(sub=sub.find(c,this.aStack));
					if(!sub)while((sub=_g.aStack[0][1]())==undefined&&_g.aStack.length>1&&_g.aStack.shift());
				}
				for(a in this.aStack[0])delete this.aStack[0][a];
			}
            function deConstruct(t,info){
                var dom=(new window.DOMParser()).parseFromString(info.source,"text/xml"),sub=dom.firstChild,oStack=[],a=null;
                while(sub){
                    if(typeof(a=oStack.length&&_g.Nodes[sub.tagName])=="function"){
                        var node=new a(sub,oStack),toS=oStack[0];node.parent=toS;
                        if(!node[0]){
                            node.children=[];
                            toS.children.push(node);
							if(toS.children.length-1)toS.children[toS.children.length-2].next=node;
                            toS.layer.appendChild(node.layer||(node.layer=tl.mD(node.type,"svg_layer")));
                            //toS.host.appendChild(node.host||(node.host=tl.mS(node.type)));
                        }
						node.init&&node.init();
                        if(node.contains&&(a=sub.firstElementChild)){oStack.unshift(node);sub=a}
                        else if(!(a=sub.nextElementSibling)){
							while(!a&&oStack.length>1){oStack.shift();a=(sub=sub.parentNode).nextElementSibling};sub=a;}
                        else sub=a;
                    }
                    else if(sub.tagName=="svg"&&!oStack.length){
                        t.name=(info.name||(t.id=sub.id)&&"#".concat(sub.id)||"untitled").concat(info.from&&" > "+info.from.name||".svg");
                        t.id=sub.id;
						t.layer=tl.mD("master","svg_layer")
                        oStack.unshift(t);
                        sub=sub.firstElementChild;
                    }
                    else{sub=sub.nextElementSibling}
                }
            }
        }).prototype=_g;
        //Sets the prototype in the same call, another advantage of anonymous constructors. Superficial but whatever. Prototype in this case is the global SVG object, being "this."
        
        (_g.Group=function(){
                
        }).prototype={
            adopt:function(){},
            mergeAll:function(){},
            applyMask:function(){}
        };
        (_g.Layer=function(){
        
        }).prototype={
            merge:function(){}
        };
		(Object.defineProperty(_g.aStack=[{}],"-1",{value:function(){for(var i=0,a={};i<arguments.length;i++)arguments[i]&&(a[i]=arguments[i]);return a},enumerable:false}))[-1].prototype=_g.aStack[0];
        _g.newGroup=function(){};
        _g.close=function(){};
        _g.save=function(){};
        _g.focus=function(){};
		_g.vPsetTarget=function(dir){
			if(typeof dir="boolean"){
				
			}
		};
		_g.animate=function(){
			var at=this.local.animationTable;
			for(var i=0;i<at.length;i++)[
				
			]
			window.webkitRequestAnimationFrame(this.animate);
		
		};
		_g.paint=function(instance,transforms){
			var s=instance||this.layer&&this,c=s.ws.node.canvas.getContext("2d"),width=c.canvas.width,height=c.canvas.height,a=null,cS=this.cStat,
			
			transforms=[1.54,0,0],
			keepX=transforms[1],
			keepY=transforms[2];
			
			//Sanitize zoom
			
			var levelplz=2,
			logZoom=Math.log(transforms[0])/Math.log(2),
			gLevel=Math.floor(logZoom),
			gSpacing=(Math.floor((logZoom-gLevel)*10))/10,
			deltaZoom=transforms[0]/cS.zoom,
			FixedZoom=Math.pow(Math.E,(gLevel+gSpacing)*Math.log(2));
			
			//Draw grid
			
			c.fillStyle="#333";
			c.strokeStyle="#777";
			c.beginPath();
			c.lineWidth=1;
			//c.rect(20,20,width-40,height-40);
			//c.fill();
			
			for(var i=.5;i<width;i+=(10+10*gSpacing)*(levelplz-gLevel)){
				c.moveTo(i,10);
				c.lineTo(i,height-20)
			}
			
			
			
			
			//cS.zoon=transforms[0];
			
			c.save();
			
			var a=null;
			
			//
			
			//cS.oX=deltaZoom*(keepX-cS.oX)-keepX;
			//cS.oY=deltaZoom*(keepY-cS.oY)-keepY;
			
			//draw rulers
			
			//var zoom=Math.pow(Math.E,(gridOffset/10)*Math.log(2))
			
			
			/*for(i0=10;i0<21;i0++){
				for(i1=0,m0=0,n=false;(m0=i0*i1+.5)<width;i1++){
					c.moveTo(m0,0);
					c.lineTo(m0,n?18*((i0-10)/10):18);
					n=!n;
				}
				c.translate(0,21)
			}*/
			log=c;
			c.stroke();
			//var zoomlog=Math.log(transforms[0])/Math.log(2);
			
			//c.scale(.5,.5)
			
			
			
			
			//c.translate(oX,oY);
			
			return;
			
			_g.aStack[0][1]=function(){console.log("Canvas construction done.")};
			//if(s.settings.ruler){}
			//draw shapes
			var sub=s.children[0],a=null;
			for(var i=0;sub&&i<10;i++){
				sub.paint&&(sub=sub.paint(c,this.aStack));
				if(!sub)while((sub=_g.aStack[0][1]())==undefined&&_g.aStack.length>1&&_g.aStack.shift());
			}
			for(a in this.aStack[0])delete this.aStack[0][a];
		};
        _g.type="svg";
    },
   ptNode:new function(){
        
    },
    openTabs:{},
    focus:null,
    envNode:new function(){//Ultra complicated HTML environment instancing that I'm replacing with the system I used for SVG.
        var m=this,
        _=function(obj){
            for(r in obj){this[r]=obj[r];}
        };_.prototype=this;
        //loginobal vDOM Methods
        m.btm=this;
        m.init=function(){
                var c=this;
                var b=c.head=c.appendChild(document.createElement('div'));
                b.className="head";
                b.host=c;
                b.contentEditable=true;
                //Analyze attributes
        };
        m.adopt=function(e,index){
            var i=this,cl=i.children.length,index=index&&index<=cl?index:cl,indexDOM=index==cl?null:i.children[index].host;
            i.children.splice(index,0,e);
            //window.e=e;
            i.insertBefore(e.host,indexDOM);
            if(e.parent)e.parent.children.splice(e.index,1);
            return e;
        };
        m.spawn=function(nType){
            var i=this,type=nType||0;
            function E(){
                var a=this,b=document.createElement(t.d);
                b.host=a;
                a.host=b;
                a.attributes={};
                a.nodeType=nType;
                a.children=[];
                a.parent;
                a.index=0;
                b.className=[t.nu,t.nf,t.nv,'tabNode'][type]+' node_';
            };
            E.prototype=m[['_undefinedHTML','_file','_element','_tabNode','_commentHTML'][type||0]];
            try{i.adopt(new E,0).init();}catch(e){return new E};
        };
        //sub Classes
        m.sClass=[
            
        
        ];
        m._undefinedHTML=new _({
            
        });
        m._commentHTML=new _({
            
        });
        m._tabNode=new _({
            hChildren:[],
            children:[],
            focus:null,
            host:[],
            init:function(){
                    var a=this;
                    a.addEventListener("keydown",this.regKeydown,false);
                    a.addEventListener("keyup",this.regKeyup,false);
                    a.addEventListener("mousedown",this.regMousedown,false);
                    a.addEventListener("mouseup",this.regMouseup,false);
                    a.spawn();
                    //a.adopt(tIDE.parseHTML(window.text));
            },
            regKeydown:function(){
                
            
            }
        });
        //DOM opperation wrapper
        for(a in {insertBefore:0,appendChild:0,addEventListener:0,removeEventListener:0}){m[a]=Function('return this.host.'+a+'.apply(this.host,arguments)')};
    }
}
function sendReq(req,callback,directory){
    //I forget why I did it this way. It's an asyncronous responce system that work in batches instead of indiviual requests. Somewhat implemented.
    //Also allows for --server-- requests becuase app constancly waits for data by way of "next request" requests that never timeout.
        var http=new XMLHttpRequest(),reqList=[],sReq="",rid;
        for(i=0;i<req.length;i++){
            req[i]=(rid=Math.floor(Math.random()*10000))+":"+req[i];
            reqList.push(rid);
        }
        http.open("POST",directory||"/",true);
        http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        http.onreadystatechange = function(){
                if(http.readyState==4&&http.status==200){
                    callback(http.response);
                }
        }
        http.send(cl(req.join("&")));
        return reqList;
}
function test(){
        //sendReq();
        $('#charmContLeft').classList.remove('collapsed');
        //$('#charmContRight').classList.remove('collapsed');
}
function initDemoSVG(){
    sendReq(["getsvg"],function(src){tIDE.workspace.newTab({source:JSON.parse(src)[a.substring(2,6)]})})

}
window.onload=function(){
	svgExampleRect="<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewbox=\"0 0 200 200\"><g><rect width=\"40\" height=\"100\" x=\"40\" style=\"fill:rgb(0,0,255);stroke-width:1px;stroke:rgb(0,0,0)\" /><rect width=\"40\" height=\"100\" x=\"100\" style=\"fill:rgb(0,0,255);stroke-width:1px;stroke:rgb(0,0,0)\" /></g></svg>"
    tIDE.io.init();
    tIDE.io.workspace.newTab({source:svgExampleRect,type:"HTML",version="5.0"}); 
    //lulz=new tIDE.svg.Instance({source:svgExampleRect})
    //sendReq(["getsvg"],tIDE.parseSVG)
    //test();
    //text=$('#temparea').childNodes[0].value;
    //console.log(text);
    //tIDE.init();               
    //console.log(document.getElementById('temparea').childNodes[0].value.split(/(?=<!--[^-]+-->|<[^>]+>)/))
}


