	tIDE.settings={
        workspace:{
            _svg:[{fI:1,type:"viewport"},{fI:2,cW:90,type:"svg_tools"},{fI:3,cW:400,type:"layers"}]
        },
		menuList:{//JSON map of menu heirarchy
			tIDE:[{
					"About tIDE":0,
					//0 does nothing, any other number will be listed in handles.
					"Report already reported bug":0
				},{
					"Launch all Zig":[{"For great Justice":0}]
				},{   
					Preferences:[{
						General:0,
						Editor:0,
						Interface:0,
						Theme:0,
						"SVG Editor":0,
					}],
					Plugins:[
						{"I haven't even finished the app...":0}
					]
				}],
			File:[{
					New:[{
						HTML:0,
						CSS:0,
						JavaScript:0,
						"Other...":0
						}],
					"New From Clipboard":[{
						"Auto Detect":0
						},{
							HTML:0,
							CSS:0,
							JavaScript:0,
							"Other...":0
						}],
					"New From Source":0
				},{
					Projects:[{
						Create:0,
						Manage:0,
						Open:0
					}],
					Browse:0
				},{
					Open:0,
					"Open Recent":0
				},{
					Save:0,
					"Save As":0
				}
			],
			Edit:[{
				Undo:0,
				Redo:0,
			},{
				"Step Backwards":0,
				"Step Forwards":0
			},{
				Cut:0,
				Copy:0,
				Clear:0,
				"Select All":0
			},{
				Paste:0,
				"Paste without Format":0
			}],
			View:[{
					"Zoom:":[],
					"Zoom in (+50%)":0,
					"Zoom out (-50%)":0
				},
				{Display:[{Preview:0}]},
				{
					"Collapse all Folds":0,
					"Expand all Folds":0,
					"Hide Selection":0,
					"Isolate Selection":0
				}
			]
		}
	}