var availElements={a:{},b:{},div:{},html:{},img:{}},t={d:'div',s:'span',nf:'node_file',nv:'node_elementV',nh:'node_elementH',nu:'node_undefinedHTML',na:'nodeHead',nb:'nodeBody',nc:'nodeFoot'};
function testparse(){
	cl($('#temparea').childNodes[0].value.split(/(?=<[^>]+>)/));
}
var tIDE={
	init:function(fileRef){
		var fileName=fileRef||"untitled";
		//$('#vpCont').appendChild((tIDE.openTabs[fileName]=tIDE.focus=tIDE.envNode.spawn.call({},3)).host);	
			
		tIDE.focus.init();
	},
	session:{
		fog:{
			loggedIn:{},
			sendReq:function(req,callback){
				var http=new XMLHttpRequest();
				http.open("POST","/tide",true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.onreadystatechange = function(){
						if(http.readyState==4&&http.status==200){
							callback(http.response);
						}
				}
				http.send(req.join("&"));
			}
		}
	},
	menuPopout:{
		init:function(parent,parentname){
			var elem=this.active=document.createElement('div'),ul,li;
			elem.classList.add("menupopup");
			elem.innerHTML='<div><ul></ul><svg viewBox="0 0 2 2" version = "1.1" class="iArrow"><polygon points="0,1 2,2 2,0" style="fill:#222;stroke-width:1"/></svg></div><svg viewBox = "0 0 2 2" version = "1.1"  class="oArrow"><polygon points="0,1 2,2 2,0" style="fill:white;stroke-width:1"/></svg>';
			ul=elem.firstElementChild.firstElementChild;
			for(n in this[parentname]){
				li=document.createElement('li');
				li.innerHTML=n;
				//li.onclick=new Function("tIDE.menuPopout."+parentname+"."+n+"(this,"+n+")");
				ul.appendChild(li);
			}
			parent.appendChild(elem);
		},
		file:{
			open:function(){
				console.log(arguments[0])
			},
			save:function(){},
			browse:function(){},
			windows:function(){}
		
		}
	},
	loginProc:{
		init:function(){
			
		},
		LoginBawx:{
			var z=this.host=document.createElement('div');
			z.className.add('popout');
			z.innerHTML='<div id="loginbawx"><div></div><form><div><input type="text" name="username" /></div><div><input type="password" name="pass"/></div></form></div>';
			this.userNameInput=z.input[1];
			this.pwdInput=z.input[2];
			
		},
		sendLogin:{
		
		},
		
	},
	filebrowser:{
		init:function(parent,type){
			//this.host=new this.FileBrowser;
			cl(this);
			console.log(parent);
		},
		FileBrowser:function(){
			var dom;
			if(tIDE.session.lastState.filebrowser){}
				dom=document.createElement('div');
				dom.innerHTML='<div id="fileBrowser"><div class="fbcontainer"><div class="fRow"><div class="fRowScrollSpring"><ul class="fRowCont"></ul></div><div class="fRowBorder"></div></div></div></div>';
			if(tIDE.session.fog.loggedIn){
				
			}
		},
		fogLS:function(a){
			sendReq([Math.floor(Math.random()*10000)+":"+"lsplz="+a],
			function(res){
				res=JSON.parse(res)
			});
		},
		dRows:[]
	},
	parseHTML:function(text){
			var sA=text.split(/(?=(<!--)|(-->)|["'<>=(]|( +))/),e,oStack=[[]];aStack=[function(){
				switch(b[0]){
					case '<':
						oStack.unshift(e=tIDE.envNode.spawn.call({},2));
						e.nodeType=b[1]||"unknown";
						aStack.unshift(attrib);
						break;
					default:
						throw new Error('Unexpected symbol ( '+b[0]+' ) in HTML Parser.');
				}
			}];
			for(i=0,b=0;b=sA.shift();i++){
				b=b.split(new RegExp("(?=.{"+(b.length-1)+"}$)"));
				aStack[0]();
			}
			console.log([oStack,aStack]);
			function attrib(){
				return function(){
					switch(b[0]){
						case '>':
							break;
							//finalize
						case ' ':
							oStack.unshift('b[1]');
							aStack.unshift(attrib2);
							break;
						default:
							throw new Error('Unexpected symbol ( '+b[0]+' ) in HTML Parser.');
					}
					return;
				}
			};
			function attrib2(){
				switch(b[0]){
					case '>':
						break;
						//finalize
					case ' ':
						break;
					default:
						throw new Error('Unexpected symbol ( '+b[0]+' ) in HTML Parser.');
				}
				return;
			};
	},	
	openTabs:{},
	focus:null,
	envNode:new function(){
		var m=this,
		_=function(obj){
			for(r in obj){this[r]=obj[r];}
		};_.prototype=this;
		//global vDOM Methods
		m.btm=this;
		m.init=function(){
				var c=this;
				var b=c.head=c.appendChild(document.createElement('div'));
				b.className="head";
				b.host=c;
				b.contentEditable=true;
				//Analyze attributes
		};
		m.adopt=function(e,index){
			var i=this,cl=i.children.length,index=index&&index<=cl?index:cl,indexDOM=index==cl?null:i.children[index].host;
			i.children.splice(index,0,e);
			//window.e=e;
			i.insertBefore(e.host,indexDOM);
			if(e.parent)e.parent.children.splice(e.index,1);
			return e;
		};
		//m.reTag=function(nodeRef){
			//var _=function(o){
			//for(r in o){this[r]=o[r];}
			//};_.prototype=nodeRef;
			//this=new _(this);			
		//}
		m.spawn=function(nType){
			var i=this,type=nType||0;
			function E(){
				var a=this,b=document.createElement(t.d);
				b.host=a;
				a.host=b;
				a.attributes={};
				a.nodeType=nType;
				a.children=[];
				a.parent;
				a.index=0;
				b.className=[t.nu,t.nf,t.nv,'tabNode'][type]+' node_';
			};
			E.prototype=m[['_undefinedHTML','_file','_element','_tabNode','_commentHTML'][type||0]];
			try{i.adopt(new E,0).init();}catch(e){return new E};
		};
		//sub Classes
		m.sClass=[
			
		
		];
		m._undefinedHTML=new _({
			
		});
		m._commentHTML=new _({
			
		});
		m._tabNode=new _({
			hChildren:[],
			children:[],
			focus:null,
			host:[],
			init:function(){
					var a=this;
					a.addEventListener("keydown",this.regKeydown,false);
					a.addEventListener("keyup",this.regKeyup,false);
					a.addEventListener("mousedown",this.regMousedown,false);
					a.addEventListener("mouseup",this.regMouseup,false);
					a.spawn();
					//a.adopt(tIDE.parseHTML(window.text));
			},
			regKeydown:function(){
				
			
			}
		});
		//DOM opperation wrapper
		for(a in {insertBefore:0,appendChild:0,addEventListener:0,removeEventListener:0}){m[a]=Function('return this.host.'+a+'.apply(this.host,arguments)')};
	}
}



function sendReq(req,callback){
		var http=new XMLHttpRequest();
		http.open("POST","/tide",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.onreadystatechange = function(){
				if(http.readyState==4&&http.status==200){
					callback(http.response);
				}
		}
		http.send(req.join("&"));
}
function test(){
		//sendReq();
		$('#charmContLeft').classList.remove('collapsed');
		//$('#charmContRight').classList.remove('collapsed');
}
window.onload=function(){
	//test();
	//text=$('#temparea').childNodes[0].value;
	//console.log(text);
	//tIDE.init();																																																			
	//console.log(document.getElementById('temparea').childNodes[0].value.split(/(?=<!--[^-]+-->|<[^>]+>)/))
}																																							
																													
