var url=require("url"),fs=require('fs'),path=require('path'),fog=require(process.cwd()+"/bin/fog.js");
exp=module.exports={
	_resource:{_default:"resource",iPad:"tablet"},
	_name:"tIDE",
	_root:function(a,b,c,d,e){
		route(a,b,c,d,e._resource);
	},
	bin:{
		_override:function(s,a,b,c,e,target){
			fog.uploadStatic(false,a,b,c,"apps/tIDE/bin",target);
		}
	}
}

function route(cs,a,b,c,d){
	fog.log=arguments;
	if(a[0].method=="POST"){
		var body="";
		a[0].on('data',function(chunk){body+=chunk});
		a[0].on('end',function(){
			parseReq(body,function(r){a[1].writeHead(200,{"Content-Type":"application/json"});
			a[1].end(r)})});
	}else{
		fog.uploadStatic(cs,a,b,c,"apps/tIDE","app.html");
	}
	function parseReq(req,callback){
		var r=req.split("&"),rl=r.length,rObj={};
		function send(r){callback(JSON.stringify(r))};
		if(!cs.user&&!(rl==1)&&!r[0].search("loginplz")+1){callback('{"command":"plzLogin"}');return}
		r.forEach(function(qvpair){
			var p=qvpair.split(/\=|\:/),b=responseHandlers[p[1]];
			(typeof b=="function"?b:function(){console.log("No '"+p[1]+"' POST handler found.")})
				(p[2],function(res){rObj[p[0]]=res;if(!--rl)send(rObj)},p[0]);
		});
	}
}

accounts={
	termtm:{
		lsdirPermit:[
			["System Root","/"],
			["TERMtm's Home","/home/termtm"],
			["TERMtm's Pictures","/home/termtm/Pictures"],
			["FOG working Directory","/home/termtm/online"]
		],
		password:"donthackmebro"
	}
};

session={lol:"wut"};

responseHandlers={
	getsvg:function(){
		fog.getFile(path.join(process.cwd(),"apps/tIDE/stuff","kinda_easy.svg"),arguments[1],function(){},true)
	},
	loginplz:function(){
		
	},
	lsplz:function(l,callback){
		var list=[];
		if(l==0){for(a=0,i=0;a=cs.dirPermitted[i];i++){
			list.push(a[0]);			
		}callback(list);
		}
	},
	searchplz:function(){},
	fileplz:function(dir,filename){},
	reqUpload:function(dir,filename){},
	upload:function(key){}
}


