function $(a){var d=document;try{return(!a.search(/^[#]/)?d.getElementById(a.slice(1)):(!a.search(/^[.]/)?d.getElementsByClassName(a.slice(1)):null))}catch(a){}}
function _(){return Function.apply(0,arguments)};
function cl(){for(i=0;i<arguments.length;i++)console.log(arguments[i]);return arguments[0]};
function forEach(b,c,d){
/*(array,function,msPerLoop)*/
    var a=(d||0),i=0,
    f=setInterval(function(){
		if(i<b.length)
			{c(b[i]);i++;}
		else
			clearInterval(f);
	},a);
}

Object.defineProperty(window,"log",{
	get:function(){return "Yeh, I can keep a secret.";},
	set:function(a){console.log(a)},
	configurable:false
});

var tl={iS:function(a){return typeof a=="string"},
	iF:function(a){return typeof a=="function"},
	iO:function(a){return typeof a=="object"},
	iN:function(a){return typeof a=="number"},
	mD:function(){var a=document.createElement("div"),i=0;for(i=0;i<arguments.length;i++)a.classList.add(arguments[i]);return a}
	