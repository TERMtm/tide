Object.defineProperty(window,"log",{get:function(){return "Yeh, I can keep a secret.";},set:function(a){console.log(a)},configurable:false});
var tl={iS:function(a){return typeof a=="string"},iF:function(a){return typeof a=="function"},
		iO:function(a){return typeof a=="object"},iN:function(a){return typeof a=="number"},
		mD:function(){var a=document.createElement("div"),i=0;for(i=0;i<arguments.length;i++)a.classList.add(arguments[i]);return a},
		mS:function(){return document.createElementNS("http://www.w3.org/2000/svg",arguments[0])},
		mR:function(a){return Math.floor(Math.random()*Math.pow(10,a||3))}},
    st={c:"class",p:"px",e:"expands",a:"active"/*String shorthand for those oodles of calls*/};

tIDE={
    init:function(fileRef){
        var fileName=fileRef||"untitled";
        tIDE.focus.init();
    },
    session:{
        fog:{
            loggedIn:{},
            sendReq:function(req,callback){
                var http=new XMLHttpRequest();
                http.open("POST","/",true);
                http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                http.onreadystatechange = function(){
                        if(http.readyState==4&&http.status==200){
                            callback(http.response);}}
                http.send(req.join("&"))}}},
    io:{
        init:function(type){
            if(!type){
                var t=this;
                t.updatesvg();
                t.updatecld();
                t.menu.init();
            }
        },
        login:{
            init:function(){
                var a=this.local;
            }
        },
        updatesvg:function(){
            //Itterates over svg placeholders set up with .innerHTML and sets up as clones of nodes stored in an <aside>
            //I gotta fix now that I figured out <use> exists.
            var a,b,c,d="";
            for(i=(c=document.getElementsByClassName("svgph")).length;a=c[--i];){
                if(b=document.getElementsByClassName("svg_"+(a.classList[1]||null))[0])a.parentNode.replaceChild(b=b.cloneNode(true),a)
                if((d=a.getAttribute(st.c).indexOf(" ",6))+1){
                    b.setAttribute(st.c,b.getAttribute(st.c).concat(a.getAttribute(st.c).slice(d)))
                }
            }
        },
        updatecld:function(){
            //Itterates over elements set up with .innerHTML and files them under respective JS objects based upon title attribute
            var b=document.getElementsByClassName("_node"),level;
            for(var i=b.length-1;i>=0;--i){
                var a=b[i],elem=a,res=[],m=false,c;
                pLoop:for(var p=0;a.title&&p<15;p++){
                    var e=a.title.split("."),lRes=[],o=0;
                    for(var n=e.length-1;n>=0;--n){
                        var f=e[n];
                        if(/^[\$\/\!\&]/.test(f)){
                            var g=f.substring(0,1);
                            f=f.substring(1);
                            //if(/^[\*]/.test(f[1]))f=this.cldSH[f.substring(1)];
                            if(g=="$"||g=="/"){
                                //Way sexier & suprisingly won in JSperf by 63%
                                Array.prototype.unshift.apply(res,m==true?f:[f,"node"]);
                                /*if(m)resolution.unshift("node");
                                resolution.unshift(f);*/
                                if(g=="$")break pLoop;
                                else m=true;
                            }
                            if(g=="&"||g=="!"){
                                if(!isNaN(f)){
                                    for(f=parseFloat(f)+1;o<f;o++){a=a.parentNode};
                                    //NEED LEXICAL SCOPING, FFS.
                                    Array.prototype.unshift.apply(res,lRes);
                                    continue pLoop;
                                }
                                else if(p==0){
                                    lRes=[f];
                                    if(g=="!")m=1;
                                }
                                else continue
                            }
                        }
                        else lRes.unshift(f);
                    }
                    Array.prototype.unshift.apply(res,lRes);
                    if(!o){a=a.parentNode};
                }
                if(!m)res.splice(1,0,"node");
                var f=res.pop(),d=tIDE;
                for(var j=0;c=res[j];j++){d=d[c]=(d.tagName?d={parent:d}:d)[c]||{}};
                if(d[f])d[f].parent=elem;else d[f]=elem;
            }
            for(var i=b.length-1;i>=0;--i){
                b[i].removeAttribute("title");
                b[i].classList.length-1?b[i].classList.remove("_node"):b[i].removeAttribute("class");
            }
            return
        },
        menu:new function(){
            //menu object containing methods and data pertinent to the menu's functionality
            var t=this;
            //Set up as an anonymous constructor for the purpose of passing "this" within closures
            t.init=function(){
            //Ties menu events to top-level nodes
                var i=t.node.item,list=tIDE.settings.menuList,c=t.node.parent;
                for(a in i){
                    i[a].level=0;
                    i[a].cList=list[a];
                    i[a].handle={click:1,mouseover:1}}
				var i=t.node.expands;
					i.tIDE.handle={click:102};
					i.Login.handle={click:101};
                t.active[-1]={cList:list};
                c.addEventListener("click",t.onEvent);
                c.addEventListener("mouseover",t.onEvent);
                c.classList.remove("off");
            };
            t.active=[];
            t.gCollect=function(p,e){
                //Deletes all menu levels above the one to be refreshed; also kills container if menu is deinitialized
                var a=t.active,p=(e=event||e)&&e.target||p,level=a.length,pL=p.level||0;
                if(level>pL){
                    for(i=pL;i<level;i++){
                    t.node.menuEnv.removeChild(a[i].cGroup);
                    a[i].classList.remove(st.a);
                    delete a[i].cGroup}
                a[pL].classList.remove(st.a);
                a.splice(pL,level-pL)}
                if(e&&e.type=="click"){
                    tIDE.io.node.overlay.removeChild(t.node.menuEnv);
                    delete t.node.menuEnv}};
            t.initCont=function(event){
                //Creates the container for menu popouts and positions them accordingly
                if(t.node.menuEnv)return;
                var aMenu=t.node.menuEnv=tl.mD();
                aMenu.id="aMenuCont";
                (aMenu.appendChild(t.node.offsetLeft=tl.mD())).classList.add("offsetLeft");
                tIDE.io.node.overlay.appendChild(aMenu);
                aMenu.addEventListener("click",t.onEvent);
                aMenu.addEventListener("mouseover",t.onEvent);
                aMenu.handle={click:2};
                t.node.container.handle={click:2};
                t.initGroup(event)};
            t.initGroup=function(event){
                //Triggered by events fired by menu elements, constucts relevant submenu and appends to container
                if(!t.node.menuEnv)return
                var a=t.active,p=event.target,level=a.length,pL=p.level||0,et=t.node.menuEnv;
                if(level>pL)t.gCollect(p);
                if(pL>1)a[pL-1].classList.add(st.a);
                /*if(!p.classList.contains(st.e)&&pL>0){return}*/
                var group=tl.mD("group"),buffer=tl.mD("buffer");
                if(pL==0){t.node.offsetLeft.style.width=p.offsetLeft+st.p}
                else{var oT=group.appendChild(tl.mD("offsetTop"));
                    oT.style.height=p.offsetTop+st.p;
                    oT.style.maxHeight=p.offsetTop+st.p;
                    oT.handle={click:2}}
                group.appendChild(buffer);
                for(i=0,d=p.cList.length;i<d;i++){
                    var k=p.cList[i];
                    if(i)(buffer.appendChild(document.createElement("hr"))).setAttribute(st.c,"splitter");
                    for(n in k){
                        var item=tl.mD();
                        item.handle={};
                        item.innerText=n;
                        if(tl.iO(k[n])&&tl.iO(k[n][0])){
                            var ctx=item.appendChild(tl.mD("ctx"));
                            ctx.appendChild(tl.mD("svgph","popout"));
                            item.classList.add(st.e);
                            item.handle.mouseover=1;
                            item.cList=k[n]
                        }
                        else if(tl.iN(k[n])){
                            item.classList.add("clickable");
                            item.handle={mouseover:2,click:3};
                        }
                        item.level=pL+1;
                        item.classList.add("item");
                        buffer.appendChild(item)
                    }
                }
                p.cGroup=group;
                if(!pL)p.classList.add(st.a);
                a.push(p);
                t.node.menuEnv.appendChild(group);
                group.handle={click:2};
                p.classList.add(st.a);
                tIDE.io.updatesvg();
            }
            t.onEvent=function(){
                //Reads event.targets event references and corresponds them with list items to run appropriate method; kills propogation, thus limiting to objects in question.
                var a=t.handles[event.type],b=event.target.handle;
                event.stopPropagation();
                if((b=(typeof b=="object")&&b[event.type])&&typeof (a=a&&a[b])=="function")a(event);
                return;
            };
            t.status={
                //Contains information that is edited upon app state change; menu constructor read these to determine exclusion, greyout, and (un)checked if applicable
                loggedin:0,
                loggedinFog:0,
                svgActive:0,
                htmlActive:0
            };
            t.handles={
                //List of functions tied to event firings
                click:{
                    0:function(){return false},
                    1:t.initCont,
                    2:t.gCollect,
					3:function(){
						var a=null;
						typeof (a=tIDE.io.menu.handles.buttons[event.target.innerText])=="function"&&a();
					},
                    101:function(event){console.log(event.target.innerHTML)},
					102:function(event){
						tIDE.io.workspace.node.parent.style.webkitTransform="translateX(100px)";
						tIDE.io.node.overlay.classList.add("menuLeft");
					}
                },
                mouseover:{
                    0:function(){return false},
                    1:t.initGroup,
                    2:t.gCollect
                },
				buttons:{
					
				}
            };   
        },
        workspace:new function(){
			t=this;
            t.tabs={};
            t.init=function(){};
            t.fTab=null;
            t.cFocus=function(instance){
                var t=this,a=null,cS=0;
                instance.focus();
				t.fTab=instance;
				
				(a=t.node[1]).firstElementChild&&a.removeChild(a.firstElementChild);
				a.appendChild(instance[1]);
				if(instance.paint)t.paint();
				return
				
                var ws=tIDE.settings.workspace._svg;
            },
            t.newTab=function(info){
                var t=this;
                while((info.idn=tl.mR(1)) in t.tabs);
                var instance=t.tabs[info.idn]=new info.Type.Instance(info);
				
				//tIDE.svg.Instance(info);
                t.node.tabzone.appendChild(instance.tab);
                //workspace=instance.wsLayout
                t.cFocus(instance);
            },
            t.onEvent=function(){
                var a=t.handles[event.type],b=event.target.handle;
                event.stopPropagation();
                if((b=(typeof b=="object")&&b[event.type])&&typeof (a=a&&a[b])=="function")a(event);
                return;
            },
            t.handles={
                click:{
                    0:function(){return false},
                    101:function(event){console.log(event.target.innerHTML)}
                },
                mouseover:{
                    0:function(){return false},
                }
            },
            t.focus=function(){};
			t.paint=function(){
				t.node.canvas.width=t.node.canvasCont.clientWidth;
				t.node.canvas.height=t.node.canvasCont.clientHeight;
				t.fTab.paint();
			};
        },
        filebrowser:{
            init:function(parent,type){
            },
            FileBrowser:function(){
                var dom;
                if(tIDE.session.lastState.filebrowser){}
                    dom=document.createElement('div');
                    dom.innerHTML='<div id="fileBrowser"><div class="fbcontainer"><div class="fRow"><div class="fRowScrollSpring"><ul class="fRowCont"></ul></div><div class="fRowBorder"></div></div></div></div>';
                if(tIDE.session.fog.loggedIn){
                    
                }
            },
            fologinS:function(a){
                sendReq([Math.floor(Math.random()*10000)+":"+"lsplz="+a],
                function(res){
                    res=JSON.parse(res)
                });
            },
            dRows:[]
        }
    }
}
window.onload=function(){
	svgExampleRect="<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewbox=\"0 0 200 200\">\n    <g><rect width=\"40\" height=\"100\" x=\"40\" style=\"fill:rgb(0,0,255);stroke-width:1px;stroke:rgb(0,0,0)\" /><rect width=\"40\" height=\"100\" x=\"100\" style=\"fill:rgb(0,0,255);stroke-width:1px;stroke:rgb(0,0,0)\" /></g>\n</svg>"
    tIDE.io.init();
    var script= document.createElement('script');
    script.type='text/javascript';
    script.src='bin/ws.text.js';
    tIDE.io.workspace.onLoadAsset=function(a){tIDE.io.workspace.newTab({source:svgExampleRect,Type:a,version:"5.0"})};
    document.getElementsByTagName('head')[0].appendChild(script);
    
}


