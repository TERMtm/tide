tIDE.io.workspace.onLoadAsset(tIDE.io.workspace.Text=new function(){
        var _g=this,_n=_g.Nodes={},aC=t.ioConstruct={},_=function(obj){for(r in obj){this[r]=obj[r]}};
		_.prototype={
			findNext:function(){
				if(this.contains&&this.children.length){
					return this.children[0];
				}
				else if(!this.next){
					var a=null;
					return a;
				}
				else return this.next;
			},
			findPos:function(){
			}
		};
		/*NODES!*/
			(_n.g=function(s){
			}).prototype=new _({type:"g",contains:true,
				paint:function(context,aStack){
					var sub=this;
					aStack.unshift(new aStack[-1](1,function(){
						return sub.parent.next
					}))
					return this.findNext();
				},
				findBounds:function(){
					
				}
			});
		/*NO MORE NODES!*/
        (_g.Instance=function(info){
            //declaration
            var t=this,viewport=t.viewport;//,t.ws=tIDE.io.workspace;
            t.children=[];
			t.ws=tIDE.io.workspace;
			t.cStat={
				zoom:1,
				oX:0,
				oY:0
			};
			t.local={
				//animate:{},
				status:{}
			};
			t.settings={ruler:true};
            if(info.source){
				deConstruct(t,info)
			}
            else {

            }
            var tE=t.tab=tl.mD("tab");
            tE.innerText=t.name;
            tE.host=t;
		
			t[1]=t.ws.node.canvasCont=tl.mD("viewport","svg");
			t[1].appendChild(tIDE.io.workspace.node.canvas=document.createElement("canvas"));
 
            function deConstruct(t,info){
                var text=info.source,lines=t.lines=text.split("\n");
                for(var i=0;i<lines.length;i++){
                	lines[i]=new t.Line(lines[i]);
                }
            }
        }).prototype=_g;



        (_g.Contructor=function(){
                
        }).prototype={
            method:function(){}
        };

        (_g.Line=function(v){
        	var a;
        	this.text=v;
        	this.indent=(a=/^\s+/.exec(this.text))?a[0].match(new RegExp(" {"+4+"}|\\t","g")).length:0;
        }).prototype={
            merge:function(){}
        };

		(Object.defineProperty(_g.aStack=[{}],"-1",{value:function(){for(var i=0,a={};i<arguments.length;i++)arguments[i]&&(a[i]=arguments[i]);return a},enumerable:false}))[-1].prototype=_g.aStack[0];
        
        _g.method=function(){};
        
        _g.close=function(){};
        _g.save=function(){};
        _g.focus=function(){};
		_g.vPsetTarget=function(dir){
			if(typeof dir=="boolean"){
				
			}
		};
		_g.animate=function(){
			var at=this.local.animationTable;
			for(var i=0;i<at.length;i++)[
				
			]
			window.webkitRequestAnimationFrame(this.animate);
		
		};
		_g.paint=function(instance,transforms){
			var s=instance||this.layer&&this,c=s.ws.node.canvas.getContext("2d"),width=c.canvas.width,height=c.canvas.height,a=null,cS=this.cStat,
			
			transforms=[1.54,0,0],
			keepX=transforms[1],
			keepY=transforms[2];
			
			//Sanitize zoom
			
			var levelplz=2,
			logZoom=Math.log(transforms[0])/Math.log(2),
			gLevel=Math.floor(logZoom),
			gSpacing=(Math.floor((logZoom-gLevel)*10))/10,
			deltaZoom=transforms[0]/cS.zoom,
			FixedZoom=Math.pow(Math.E,(gLevel+gSpacing)*Math.log(2));
			
			//Draw grid
			
			c.fillStyle="#333";
			c.strokeStyle="#777";
			c.beginPath();
			c.lineWidth=1;
			
			for(var i=.5;i<width;i+=(10+10*gSpacing)*(levelplz-gLevel)){
				c.moveTo(i,10);
				c.lineTo(i,height-20)
			}
			
			return;
			
			_g.aStack[0][1]=function(){console.log("Canvas construction done.")};
			
			//draw shapes
			var sub=s.children[0],a=null;
			for(var i=0;sub&&i<10;i++){
				sub.paint&&(sub=sub.paint(c,this.aStack));
				if(!sub)while((sub=_g.aStack[0][1]())==undefined&&_g.aStack.length>1&&_g.aStack.shift());
			}
			for(a in this.aStack[0])delete this.aStack[0][a];
		};
        _g.type="text";
    }
);