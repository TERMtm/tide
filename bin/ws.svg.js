new function(){
	
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Global Setup ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
    
    var _g=this,
    	_n=_g.Nodes={},
    	aC=t.ioConstruct={};

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Workspace Info ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    _g.type="svg";
    _g.build="0";
    _g.lastBuildDate="Tue, 02 Apr 2013 21:20:07 GMT";

    	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Instance Constructor ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    (_g.Instance=function(info){
        //declaration
        var t=this,viewport=t.viewport;//,t.ws=tIDE.io.workspace;
        t.children=[];
		t.ws=tIDE.io.workspace;
		t.cStat={
			zoom:1,
			oX:0,
			oY:0
		};
		t.local={
			//animate:{},
			status:{}
		};
		t.settings={ruler:true};
        if(info.source){
			deConstruct(t,info)
			
		}
        else {

        }
        var tE=t.tab=tl.mD("tab");
        tE.innerText=t.name;
        tE.host=t;
	
		t[1]=t.ws.node.canvasCont=tl.mD("viewport","svg");
		t[1].appendChild(tIDE.io.workspace.node.canvas=document.createElement("canvas"));

		function getBounds(){
			var sub=s.children[0],a=null;
			for(var i=0;sub&&i<10;i++){
				sub.findPos&&(sub=sub.find(c,this.aStack));
				if(!sub)while((sub=_g.aStack[0][1]())==undefined&&_g.aStack.length>1&&_g.aStack.shift());
			}
			for(a in this.aStack[0])delete this.aStack[0][a];
		}
        function deConstruct(t,info){
            var dom=(new window.DOMParser()).parseFromString(info.source,"text/xml"),sub=dom.firstChild,oStack=[],a=null;
            while(sub){
                if(typeof(a=oStack.length&&_g.Nodes[sub.tagName])=="function"){
                    var node=new a(sub,oStack),toS=oStack[0];node.parent=toS;
                    if(!node[0]){
                        node.children=[];
                        toS.children.push(node);
						if(toS.children.length-1)toS.children[toS.children.length-2].next=node;
                        toS.layer.appendChild(node.layer||(node.layer=tl.mD(node.type,"svg_layer")));
                        //toS.host.appendChild(node.host||(node.host=tl.mS(node.type)));
                    }
					node.init&&node.init();
                    if(node.contains&&(a=sub.firstElementChild)){oStack.unshift(node);sub=a}
                    else if(!(a=sub.nextElementSibling)){
						while(!a&&oStack.length>1){oStack.shift();a=(sub=sub.parentNode).nextElementSibling};sub=a;}
                    else sub=a;
                }
                else if(sub.tagName=="svg"&&!oStack.length){
                    t.name=(info.name||(t.id=sub.id)&&"#".concat(sub.id)||"untitled").concat(info.from&&" > "+info.from.name||".svg");
                    t.id=sub.id;
					t.layer=tl.mD("master","svg_layer")
                    oStack.unshift(t);
                    sub=sub.firstElementChild;
                }
                else{sub=sub.nextElementSibling}
            }
        }
    }).prototype=_g;

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Global SVG Methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
        
    (_g.Group=function(){
            
    }).prototype={
        adopt:function(){},
        mergeAll:function(){},
        applyMask:function(){}
    };
    (_g.Layer=function(){
    
    }).prototype={
        merge:function(){}
    };
	(Object.defineProperty(_g.aStack=[{}],"-1",{value:function(){for(var i=0,a={};i<arguments.length;i++)arguments[i]&&(a[i]=arguments[i]);return a},enumerable:false}))[-1].prototype=_g.aStack[0];
    _g.newGroup=function(){};
    _g.close=function(){};
    _g.save=function(){};
    _g.focus=function(){};
	_g.vPsetTarget=function(dir){
		if(typeof dir="boolean"){
			
		}
	};
	_g.animate=function(){
		var at=this.local.animationTable;
		for(var i=0;i<at.length;i++)[
			
		]
		window.webkitRequestAnimationFrame(this.animate);
	
	};
	_g.paint=function(instance,transforms){
		var s=instance||this.layer&&this,c=s.ws.node.canvas.getContext("2d"),width=c.canvas.width,height=c.canvas.height,a=null,cS=this.cStat,
		
		transforms=[1.54,0,0],
		keepX=transforms[1],
		keepY=transforms[2];
		
		//Sanitize zoom
		
		var levelplz=2,
		logZoom=Math.log(transforms[0])/Math.log(2),
		gLevel=Math.floor(logZoom),
		gSpacing=(Math.floor((logZoom-gLevel)*10))/10,
		deltaZoom=transforms[0]/cS.zoom,
		FixedZoom=Math.pow(Math.E,(gLevel+gSpacing)*Math.log(2));
		
		//Draw grid
		
		c.fillStyle="#333";
		c.strokeStyle="#777";
		c.beginPath();
		c.lineWidth=1;
		
		for(var i=.5;i<width;i+=(10+10*gSpacing)*(levelplz-gLevel)){
			c.moveTo(i,10);
			c.lineTo(i,height-20)
		}
		
		return;
		
		_g.aStack[0][1]=function(){console.log("Canvas construction done.")};
		
		//draw shapes
		var sub=s.children[0],a=null;
		for(var i=0;sub&&i<10;i++){
			sub.paint&&(sub=sub.paint(c,this.aStack));
			if(!sub)while((sub=_g.aStack[0][1]())==undefined&&_g.aStack.length>1&&_g.aStack.shift());
		}
		for(a in this.aStack[0])delete this.aStack[0][a];
	};

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Node Setup ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	var_=function(obj){for(r in obj){this[r]=obj[r]}};
	_.prototype={
		findNext:function(){
			if(this.contains&&this.children.length){
				return this.children[0];
			}
			else if(!this.next){
				var a=null;
				return a;
			}
			else return this.next;
		},
		findPos:function(){
		}
	};

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Node Declaration ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		
	(_n.g=function(s){
	}).prototype=new _({type:"g",contains:true,
		paint:function(context,aStack){
			var sub=this;
			aStack.unshift(new aStack[-1](1,function(){
				return sub.parent.next
			}))
			return this.findNext();
		},
		findBounds:function(){
			
		}
	});
	(_n.path=function(s){
		var t=this;

		t.points=s.pathSegList;
		t.array=[];
		for(var i=0,j=t.points.numberOfItenms;i<j;i++)t.array.push(t.points.getItem(i));

	}).prototype=new _({type:"path"});
	(_n.polygon=function(){
	}).prototype=new _({type:"polygon"});
	(_n.rect=function(s){
		var t=this;
		t.stat=[s.x.baseVal.value,s.y.baseVal.value,s.width.baseVal.value,s.height.baseVal.value,s.rx.baseVal.value,s.ry.baseVal.value];
		t.layer=tl.mD("g","svg_layer");
	}).prototype=new _({type:"rect",
		paint:function(context,aStack){
			//context.rect.apply(context,this.stat.slice(0,4))
			t=this;
			if(this.animation.length){
				for(var i=0;i<this.animation.length;i++){
					
				}
			}
			context.rect(t.stat[0],t.stat[1],t.stat[2],t.stat[3])
			context.fillStyle="#333";
			context.fill();
			return this.findNext();
		}
	
	
	});
	(_n.ellipse=function(){}).prototype=new _({type:"ellipse"});
	(_n.circle=function(s){
		var t=this;
		t.stat=[t.cx=s.cx.baseVal.value,t.cy=s.cy.baseVal.value,t.r=s.r.baseVal.value];

	}).prototype=new _({type:"circle"});
	(_n.text=function(){}).prototype=new _({type:"text"});
	(_n.image=function(){}).prototype=new _({type:"image"});
	(_n.line=function(){}).prototype=new _({type:"line"});
	(_n.linearGradient=function(){if(oStack[0].type="defs"){
		}return sub.nextElementSibling
	}).prototype=new _({type:"linearGradient"});
	(_n.defs=function(){}).prototype=new _({type:"defs",contains:true});
	(_n.desc=function(){}).prototype=new _({type:"desc",contains:true});
	(_n.metadata=function(){}).prototype=new _({type:"metadata",contains:true});
	(_n.polyline=function(){}).prototype=new _({type:"polyline"});
	(_n.stop=function(){}).prototype=new _({type:"stop"});
	(_n.style=function(){}).prototype=new _({type:"style"});
	(_n["switch"]=function(){}).prototype=new _({type:"switch"});
	(_n.symbol=function(){}).prototype=new _({type:"symbol"});
	(_n.tref=function(){}).prototype=new _({type:"tref"});
	(_n.tspan=function(){}).prototype=new _({type:"tspan"});
	(_n.title=function(){}).prototype=new _({type:"title"});
	(_n["use"]=function(){}).prototype=new _({type:"use"});
	(_n.mask=function(){}).prototype=new _({type:"mask",contains:true});


		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Executions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

	tIDE.io.workspace.onLoadAsset(tIDE.io.workspace[this.type]=this);
    
}